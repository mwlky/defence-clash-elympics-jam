using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastleTowerView : MonoBehaviour
{
    [SerializeField] private GameObject leftTower;
    [SerializeField] private GameObject rightTower;

    public void SetRightTowerView(Sprite sprite)
    {
        var spriteRenderer = rightTower.AddComponent<SpriteRenderer>();

        spriteRenderer.sprite = sprite;
    }

    public void SetLeftTowerView(Sprite sprite)
    {
        var spriteRenderer = leftTower.AddComponent<SpriteRenderer>();

        if (!spriteRenderer) return;

        spriteRenderer.sprite = sprite;
    }
}