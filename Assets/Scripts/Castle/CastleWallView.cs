using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastleWallView : MonoBehaviour
{
    
    public void SetWallView(Sprite sprite)
    {
        var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();

        spriteRenderer.sprite = sprite;
        spriteRenderer.sortingOrder = 1;
    }
}
