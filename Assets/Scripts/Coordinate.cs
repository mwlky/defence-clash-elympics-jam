using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class Coordinate : MonoBehaviour
{
    private Vector2Int coordinates = new Vector2Int();

    private void Update()
    {
        if (!Application.isPlaying)
            SetObjectNameToCoordinates();
    }

    private void SetObjectNameToCoordinates()
    {
        coordinates.x = Mathf.RoundToInt(transform.position.x);
        coordinates.y = Mathf.RoundToInt(transform.position.y);

        gameObject.name = $"{coordinates.x} , {coordinates.y}";
    }
}