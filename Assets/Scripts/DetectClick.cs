using System;
using System.Collections;
using System.Collections.Generic;
using Elympics;
using UnityEngine;

public class DetectClick : ElympicsMonoBehaviour, IObservable, IInputHandler
{
    public static event Action<Tile> onSpawnUnit;

    [SerializeField] private Color onHoverColor;
    private Color baseColor;
    private Tile currentTile;
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        currentTile = GetComponentInParent<Tile>();
    }

    public void OnInputForBot(IInputWriter inputSerializer)
    {
    }

    public void OnInputForClient(IInputWriter inputSerializer)
    {
    }
}