using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class UnitUI : MonoBehaviour
{
    public static event Action<int> OnUnitSelected;

    [SerializeField]
    public UnitData unit;
    [SerializeField]
    private TextMeshProUGUI costTmp;
    [SerializeField]
    private Image unitType;
    [SerializeField]
    private Image levelUp;
    [SerializeField]
    private GameObject[] stars;
    
    private Button button;
    private Image frame;
    private int starsCount = 0;
    private void OnEnable()
    {
        SpawnManager.OnUnitSpawned += UncheckUnit;
    }

    private void OnDisable()
    {
        SpawnManager.OnUnitSpawned -= UncheckUnit;
        
    }

    private void Start()
    {
        SetUpCard();
        
    }

    private void SetUpCard()
    {
        button = GetComponent<Button>();
        unitType.sprite = unit.sprite;
        costTmp.text = unit.unitCost.ToString();
        frame = GetComponent<Image>();
        button.onClick.AddListener(CheckUnit);
    }

    public void LevelUp()
    {
        if (starsCount <= stars.Length)
        {
            stars[starsCount].SetActive(true);
            starsCount++;
            levelUp.gameObject.SetActive(false);
        }       
    }
    private void CheckUnit()
    {
        OnUnitSelected?.Invoke(unit.ID);
        frame.color = Color.green;
    }
    private void UncheckUnit(int value)
    {

        frame.color = Color.white;
    }
}
