using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Site
{
    Left,
    Middle,
    Right
};

public enum PlayerIndex
{
    Player0,
    Player1
};
