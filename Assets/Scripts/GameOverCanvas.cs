using System;
using System.Collections;
using System.Collections.Generic;
using Elympics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverCanvas : ElympicsMonoBehaviour
{
    [SerializeField] private GameObject gameWinImage;
    [SerializeField] private GameObject gameLoseImage;
    [SerializeField] private GameObject blackcout;
    [SerializeField] private Button exitGameButton;

    private ElympicsPlayer currentPlayer;

    private void OnEnable()
    {
        HealthController.OnGameFinished += ShowGameOverCanvas;
        exitGameButton.onClick.AddListener(ExitGame);
    }

    void ExitGame()
    {
        SceneManager.LoadScene("Scenes/Menu");
    }
    
    void ShowGameOverCanvas(int playerLoseIndex)
    {
        if (Elympics.Player == ElympicsPlayer.FromIndex(playerLoseIndex))
            gameLoseImage.SetActive(true);

        else
            gameWinImage.SetActive(true);

        blackcout.SetActive(true);
        exitGameButton.gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        HealthController.OnGameFinished -= ShowGameOverCanvas;
        exitGameButton.onClick.RemoveListener(ExitGame);
    }
}