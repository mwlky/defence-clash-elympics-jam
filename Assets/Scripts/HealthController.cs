using System;
using System.Collections;
using System.Collections.Generic;
using Elympics;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class HealthController : ElympicsMonoBehaviour, IObservable
{
    public static event Action<int> OnGameFinished;
    public static event Action<Unit> OnUnitKilled;
    [SerializeField] private TextMeshProUGUI textToUpdate;

    [SerializeField] float maxHealth = 100;
    private ElympicsFloat health = new ElympicsFloat(0);

    public int predictableOfParent;

    [SerializeField] private ElympicsBool isNexus = new ElympicsBool();
    [SerializeField] private ElympicsBool isUnit = new ElympicsBool();

    void Start()
    {
        predictableOfParent = (int)gameObject.GetComponentInParent<ElympicsMonoBehaviour>().PredictableFor;

        health.Value = maxHealth;
        UpdateText();
    }

    private void OnParticleCollision(GameObject other)
    {
        if (!other.CompareTag("Bullet")) return;

        var isParticleSameAsUs = other.gameObject.GetComponent<ElympicsBehaviour>()
            .IsPredictableTo(ElympicsPlayer.FromIndex(predictableOfParent));

        if (!isParticleSameAsUs)
        {
            UpdateHealth();
            other.GetComponent<ParticleSystem>().Clear();
        }
    }

    void UpdateHealth()
    {
        health.Value -= 10;

        Mathf.Clamp(health.Value, 0, maxHealth);

        UpdateText();

        if (health.Value > 0) return;

        if (isNexus.Value)
            OnGameFinished?.Invoke(predictableOfParent);

        KillUnit();
        
        
    }

    private void KillUnit()
    {
        gameObject.SetActive(false);
        Unit unit = GetComponent<Unit>();
        if(unit!=null)
        OnUnitKilled(unit);
    }

    void UpdateText()
    {
        if (textToUpdate == null) return;
        textToUpdate.text = health.Value.ToString();
    }
}