using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Elympics;
using System;

[RequireComponent(typeof(InputProvider))]
public class InputController : ElympicsMonoBehaviour, IInputHandler, IInitializable, IUpdatable
{
    public static InputController Instance;
    public event Action<bool, Vector2> OnMouseClick;
    private InputProvider inputProvider;
    [SerializeField] private SpawnManager spawnManager;
    [SerializeField] private int playerID;
    private Vector2 position;
    private int currentTileID;
    private int unitID;

    public void Initialize()
    {
        inputProvider = GetComponent<InputProvider>();
    }

    public void ElympicsUpdate()
    {
        currentTileID = -1;
        unitID = -1;
        if (ElympicsBehaviour.TryGetInput(PredictableFor, out var inputDeserializer))
        {
            inputDeserializer.Read(out currentTileID);
            inputDeserializer.Read(out unitID);
        }

        ProcessTile(currentTileID,unitID, PredictableFor);
    }

    private void ProcessUnit(int unitID, ElympicsPlayer inputFromPlayer)
    {        
        spawnManager.selectedUnitID.Value = unitID;
        Debug.Log($"selected unit is: {unitID}");
    }

    private void ProcessMouse(bool mouseClicked, Vector2 mousePosition)
    {
        OnMouseClick?.Invoke(mouseClicked, mousePosition);
    }

    private void ProcessTile(int tileID,int unitID, ElympicsPlayer inputFromPlayer)
    {
        spawnManager.SpawnUnit(tileID,unitID, inputFromPlayer);
    }

    public void OnInputForBot(IInputWriter inputSerializer)
    {
        //todo
    }

    public void OnInputForClient(IInputWriter inputSerializer)
    {
        if (Elympics.Player == ElympicsPlayer.FromIndex(playerID))
            SerializeInput(inputSerializer);
    }

    private void SerializeInput(IInputWriter inputWriter)
    {
        inputWriter.Write(inputProvider.currentTileID);
        inputWriter.Write(inputProvider.UnitID);
    }
}