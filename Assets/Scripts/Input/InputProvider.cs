using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Elympics;
using System;

public class InputProvider : MonoBehaviour
{
    public bool MouseClicked => _mouseClicked;
    private bool _mouseClicked = false;
    public Vector2 MousePosition => _mousePosition;
    private Vector2 _mousePosition = Vector2.zero;

    public int currentTileID => _currentTileID;
    [SerializeField] private int _currentTileID;

    public int UnitID => _unitID; 
    [SerializeField]
    private int _unitID = -1;
    private void Awake()
    {
        AddListeners();
    }
    private void OnDisable()
    {
        RemoveListeners();
    }



    private void Update()
    {
        
        _mousePosition = Input.mousePosition;
      

        _mouseClicked = Input.GetMouseButtonDown(0);
    }
    private void OnTileClicked(int tileID)
    {
        _currentTileID = tileID;
    }
    private void OnUnitSelected(int unitID)
    {
        _unitID = unitID;
    }

    private void AddListeners()
    {
        Tile.OnTileClicked += OnTileClicked;
        UnitUI.OnUnitSelected += OnUnitSelected;
    }



    private void RemoveListeners()
    {
        Tile.OnTileClicked -= OnTileClicked;
        UnitUI.OnUnitSelected -= OnUnitSelected;
    }
}
