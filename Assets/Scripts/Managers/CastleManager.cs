using System;
using System.Collections;
using System.Collections.Generic;
using Elympics;
using UnityEngine;

public class CastleManager : ElympicsMonoBehaviour
{
    [Header("Stones")]
    [SerializeField] private GameObject leftStones;
    [SerializeField] private GameObject rightStones;
    [SerializeField] private GameObject middleStones;

    [Header("Castles")]
    [SerializeField] private GameObject[] castleOnTheLeft;
    [SerializeField] private GameObject[] castleOnTheMiddle;
    [SerializeField] private GameObject[] castleOnTheRight;

    [Header("Sprites")]
    [SerializeField] private Sprite[] wallSprites;
    [SerializeField] private Sprite[] leftTowerSprites;
    [SerializeField] private Sprite[] rightTowerSprites;
    [SerializeField] private Sprite[] leftStonesSprite;
    [SerializeField] private Sprite[] middleStonesSprite;
    [SerializeField] private Sprite[] rightStonesSprite;

    public void SetLeftSide(int generatedNumber)
    {
        foreach (var wall in castleOnTheLeft)
        {
            wall.GetComponent<CastleWallView>().SetWallView(wallSprites[generatedNumber]);
            SetBothTowersView(wall, generatedNumber);
        }
        SetLeftStonesView(generatedNumber);
    }

    public void SetRightSide(int generatedNumber)
    {
        foreach (var wall in castleOnTheRight)
        {
            wall.GetComponent<CastleWallView>().SetWallView(wallSprites[generatedNumber]);
            SetBothTowersView(wall, generatedNumber);
        }
        SetRightStonesView(generatedNumber);
    }

    public void SetMiddleSide(int generatedNumber)
    {
        foreach (var wall in castleOnTheMiddle)
        {
            wall.GetComponent<CastleWallView>().SetWallView(wallSprites[generatedNumber]);
            SetBothTowersView(wall, generatedNumber);
        }
        
        SetMiddleStonesView(generatedNumber);
    }

    public void SetLeftStonesView(int randomNumber)
    {
        leftStones.GetComponent<StoneViews>().SetStoneView(leftStonesSprite[randomNumber]);
    }
    
    public void SetRightStonesView(int randomNumber)
    {
        rightStones.GetComponent<StoneViews>().SetStoneView(rightStonesSprite[randomNumber]);
    }
    
    public void SetMiddleStonesView(int randomNumber)
    {
        middleStones.GetComponent<StoneViews>().SetStoneView(middleStonesSprite[randomNumber]);
    }

    private void SetLeftTower(GameObject wall, int generatedNumber)
    {
        var component = wall.GetComponent<CastleTowerView>();
        component.SetLeftTowerView(leftTowerSprites[generatedNumber]);
    }

    private void SetRightTower(GameObject wall, int generatedNumber)
    {
        var component = wall.GetComponent<CastleTowerView>();
        component.SetRightTowerView(rightTowerSprites[generatedNumber]);
    }

    private void SetBothTowersView(GameObject wall, int generatedNumber)
    {
        SetRightTower(wall, generatedNumber);
        SetLeftTower(wall, generatedNumber);
    }
}