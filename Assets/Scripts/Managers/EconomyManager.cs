using Elympics;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EconomyManager : ElympicsMonoBehaviour
{
    public static event Action<ElympicsInt, int> OnMoneyDown;
    [SerializeField]
    private int playerID;
    [SerializeField]
    public ElympicsInt moneyAmount = new ElympicsInt(1000);
    [SerializeField]
    private UnitUI[] units;
    private UnitData[] unitData = new UnitData[3];

    [SerializeField]
    private TextMeshProUGUI text;
    private void Start()
    {
        SetUnitData();
        UpdateUI(0, 0);
    }

  

    private void OnEnable()
    {
        SpawnManager.OnUnitSpawned += OnUnitSpawned;
        HealthController.OnUnitKilled += OnUnitKilled;
        moneyAmount.ValueChanged += UpdateUI;
    }

    private void UpdateUI(int lastValue, int newValue)
    {
        if(playerID == (int)Elympics.Player&& moneyAmount > 0)
        text.text = moneyAmount.ToString();
        OnMoneyDown(moneyAmount, playerID);
    }

    private void OnDisable()
    {
        SpawnManager.OnUnitSpawned -= OnUnitSpawned;
    }
    private void SetUnitData()
    {
        for (int i = 0; i < units.Length; i++)
        {
            unitData[i] = units[i].unit;
        }
    }
    private void OnUnitKilled(Unit unit)
    {
        
        moneyAmount.Value += unit.unitData.unitCost;
    }

    private void OnUnitSpawned(int unitID)
    {
        for (int i = 0; i < units.Length; i++)
        {
            if(unitData[i].ID == unitID)
            {
                moneyAmount.Value -= unitData[i].unitCost;
            }
        }
        // if (moneyAmount > data.unitCost)
        //    moneyAmount -= data.unitCost;
    }
}

