using System;
using System.Collections;
using System.Collections.Generic;
using Elympics;
using UnityEngine;

public class GameManager : ElympicsMonoBehaviour, IObservable
{
    [SerializeField] private GameObject gameOverCanvas;

    private void OnEnable()
    {
        HealthController.OnGameFinished += OnGameFinished;
    }

    void OnGameFinished(int playerNexusIsDead)
    {
    }

    private void OnDisable()
    {
        HealthController.OnGameFinished -= OnGameFinished;
    }
}