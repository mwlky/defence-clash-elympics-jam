using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Elympics;
using UnityEngine;
using Random = UnityEngine.Random;

[System.Serializable]
public class TileSprites
{
    public Sprite[] spritesTextures;
}

public class GridManager : ElympicsMonoBehaviour, IObservable, IInitializable
{
    [SerializeField] private CastleManager castleManager;

    public TileSprites[] sprites;

    [SerializeField] private GameObject[] tilesOnTheLeft;
    [SerializeField] private GameObject[] tilesOnTheMiddle;
    [SerializeField] private GameObject[] tilesOnTheRight;

    [HideInInspector] public int randomNumber;

    private ElympicsInt randomSeed = new ElympicsInt(0);

    private void Start()
    {
        if (Elympics.IsServer)
            randomSeed.Value = Random.Range(-1000000, 1000000);
    }

    public void Initialize() => randomSeed.ValueChanged += GenerateRandomTiles;

    private void GenerateRandomTiles(int oldValue, int newValue)
    {
        List<int> generatedNumbers = new List<int>();

        int range = sprites.Length;
        System.Random random = GenerateNumber(newValue, range);
        generatedNumbers.Add(randomNumber);

        IterateOnEachTile(tilesOnTheLeft, randomNumber);
        castleManager.SetLeftSide(randomNumber);

        CheckIfRandomNumberRepeatedAndRandomizeAgain(random, range, generatedNumbers);

        IterateOnEachTile(tilesOnTheRight, randomNumber);
        castleManager.SetRightSide(randomNumber);

        CheckIfRandomNumberRepeatedAndRandomizeAgain(random, range, generatedNumbers);

        IterateOnEachTile(tilesOnTheMiddle, randomNumber);
        castleManager.SetMiddleSide(randomNumber);
    }

    private void CheckIfRandomNumberRepeatedAndRandomizeAgain(System.Random random, int range, List<int> generatedNumbers)
    {
        do
        {
            randomNumber = random.Next(0, range);
        } while (generatedNumbers.Contains(randomNumber));

        generatedNumbers.Add(randomNumber);
    }

    System.Random GenerateNumber(int newValue, int range)
    {
        System.Random random = new System.Random(newValue);
        range = sprites.Length;
        randomNumber = random.Next(0, range);

        return random;
    }

    private void IterateOnEachTile(GameObject[] tile, int number)
    {
        foreach (GameObject _tile in tile)
        {
            ReplaceTile(number, _tile);
        }
    }

    private void ReplaceTile(int randomNumber, GameObject tile)
    {
        TileView component = tile.GetComponent<TileView>();
        component.SetTileView(sprites[randomNumber].spritesTextures);
    }
}