using System;
using System.Collections;
using System.Collections.Generic;
using Elympics;
using UnityEngine;

public class SpawnManager : ElympicsMonoBehaviour, IObservable
{
    public static event Action<int> OnUnitSpawned;

    public ElympicsInt selectedUnitID = new ElympicsInt(-1);

    private int player0Money;
    private int player1Money;

    [SerializeField] private Tile[] allTiles;
    private void OnEnable()
    {
        EconomyManager.OnMoneyDown += UpdateMoney;
    }

    private void UpdateMoney(ElympicsInt money, int ID)
    {
        if(ID==0)
            player0Money = money;
        else
            player1Money = money;
    }

    private void OnDestroy()
    {
        
    }
    public void SpawnUnit(int tileID,int unitID, ElympicsPlayer inputPlayer)
    {
        foreach (Tile tile in allTiles)
        {
            if (tile.ID == tileID && inputPlayer == tile.PredictableFor && tile.isEmpty )
            {
               // if((int)Elympics.Player==0&& player0Money > 0 || (int)Elympics.Player == 1 && player1Money > 0)                  
                    {
                        tile.SpawnUnit(unitID);   //

                        selectedUnitID.Value = -1;
                        OnUnitSpawned?.Invoke(unitID);
                    }
               
            }
        }
    }
}

