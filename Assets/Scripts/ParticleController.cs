using System;
using System.Collections;
using System.Collections.Generic;
using Elympics;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    [SerializeField] private LayerMask unit1;
    [SerializeField] private LayerMask unit0;
    [SerializeField] private LayerMask wall0;
    [SerializeField] private LayerMask wall1;
    
    private void Start()
    {
        var parentPredictable = (int)gameObject.GetComponentInParent<ElympicsMonoBehaviour>().PredictableFor;
        SetColliders(parentPredictable);
    }

    void SetColliders(int predictable)
    {
        // var particle = GetComponent<ParticleSystem>();
        // var collision = particle.collision;
        // LayerMask unitLayer;
        // LayerMask wallLayer;
        //
        // if (predictable == 0)
        // {
        //     unitLayer = unit1;
        //     wallLayer = wall1;
        // }
        //
        // else
        // {
        //     unitLayer = unit0;
        //     wallLayer = wall0;
        // }
        //
        // collision.collidesWith = unitLayer;
        // collision.collidesWith = wallLayer;
        //
        // Debug.Log(collision.collidesWith);
    }
}