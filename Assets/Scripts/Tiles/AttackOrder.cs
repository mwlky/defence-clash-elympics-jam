using System;
using System.Collections;
using System.Collections.Generic;
using Elympics;
using UnityEngine;

public class AttackOrder : ElympicsMonoBehaviour, IUpdatable
{
    [SerializeField] private Tile[] tilesOrderCheck;
    private Tile tileParent;
    public Unit unitToAttack;

    private void Awake()
    {
        tileParent = gameObject.GetComponent<Tile>();
    }

    public void ElympicsUpdate()
    {
        FindEnemy();
    }

    private void FindEnemy()
    {
        if (!tileParent.unitOnTile.Value) return;

        foreach (Tile tile in tilesOrderCheck)
        {
            if (!tile.isEmpty.Value)
            {
                unitToAttack = tile.unitOnTile.Value.GetComponent<Unit>();
                break;
            }
        }
    }
}