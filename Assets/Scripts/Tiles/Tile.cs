using System;
using System.Collections;
using System.Collections.Generic;
using Elympics;
using UnityEngine;

public class Tile : ElympicsMonoBehaviour, IObservable
{
    private AttackOrder order;

    public static event Action<int> OnTileClicked;

    public ElympicsGameObject unitOnTile = new ElympicsGameObject();
    public ElympicsGameObject unitToAttack = new ElympicsGameObject();
   

    public int ID = 0;
    public ElympicsBool isEmpty = new ElympicsBool(true);

    private void Awake()
    {
        order = gameObject.GetComponent<AttackOrder>();
    }
    private void OnEnable()
    {
        HealthController.OnUnitKilled += OnUnitKilled;   
    }

    private void OnUnitKilled(Unit unit)
    {
        if (unit == unitOnTile)
        {
            ClearTile();
        }
    }

    private void ClearTile()
    {
        unitOnTile = null;
        unitToAttack = null;
        isEmpty.Value = true;
    }

    private void OnDisable()
    {
        HealthController.OnUnitKilled -= OnUnitKilled;
    }
    private void OnMouseDown()
    {
        if (isEmpty)
        {
            OnTileClicked(ID);
            isEmpty.Value = false;
        }
    }

    public void SpawnUnit(int unitID) //
    {
        GameObject unit;
        Unit unitComponent;
        if (!isEmpty) return;

        if(unitID >= 0)
        {
            unit = transform.GetChild(unitID).gameObject; //
            unitComponent = unit.GetComponent<Unit>();

            unitComponent.tileThatUnitStaysOn = this;
            unitOnTile.Value = unit.GetComponent<ElympicsBehaviour>();

            isEmpty.Value = false;
            unit.SetActive(true);
        }
        
        // unitToAttack = order.unitToAttack;
    }
}