using System;
using System.Collections;
using System.Collections.Generic;
using Elympics;
using UnityEngine;
using Random = UnityEngine.Random;

public class TileView : ElympicsMonoBehaviour, IObservable, IInitializable
{
    private ElympicsInt randomSeed = new ElympicsInt(0);

    private int randomNumber = 0;

    // Set exact number as lenght of spriteTextures[] in GridManager class
    private int tilesNumber = 24;

    private void Start()
    {
        if (Elympics.IsServer)
            randomSeed.Value = Random.Range(-100000, 00000);
    }

    public void Initialize()
    {
        randomSeed.ValueChanged += GenerateNumber;
    }

    void GenerateNumber(int oldValue, int newValue)
    {
        System.Random random = new System.Random(newValue);
        int range = tilesNumber;
        randomNumber = random.Next(0, range);
    }

    public void SetTileView(Sprite[] sprites)
    {
        var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();

        spriteRenderer.sortingOrder = -1;
        spriteRenderer.sprite = sprites[randomNumber];
    }
}