using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Unit",menuName ="Units")]
public class UnitData : ScriptableObject
{
    public string name;
    public int ID;
    public Sprite sprite;
    public int unitCost;
    public int maxHealth;
    public float demage;
}
