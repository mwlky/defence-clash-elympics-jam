using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneViews : MonoBehaviour
{
    public void SetStoneView(Sprite sprite)
    {
        var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();

        spriteRenderer.sortingOrder = 2;
        spriteRenderer.sprite = sprite;
    }
    
}
