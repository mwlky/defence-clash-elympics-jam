![Platforms](https://img.shields.io/badge/Platform-Unity2021.3.5f1-black.svg?logo=unity&style=flat)
![Language](https://img.shields.io/badge/Language-CSharp-239120.svg?logo=csharp&style=flat)
[![Discord](https://img.shields.io/badge/Discord-Elympics-white?logo=Discord&style=flat)](https://discord.gg/Xh6AR2mceg)

# ElympicsJAM

Hello there! Welcome, it's great to see you. This is a starter project for **Elympcis JAM** hosted by [Elympics](https://elympics.cc/). I hope you're having a great day and that you're in the mood to learn new things! 💪👩‍🏫

## What will I be doing?

Your goal for the next two weeks is to implement a **competitive** 💪 or **cooperative** 🤝, **multiplayer** 👫 game with esports grade realtime experience, that is ready to be run in the cloud. That will all be possible thanks to [Elympics Unity SDK](https://github.com/Elympics/Unity-SDK).

## What is Elympics?

Elympics is a new category of **multiplayer game development platform** dedicated especially to **competitive casual multiplayer** games with **play&earn elements**.

It provides indie developers, small and medium game studios with off-the-shelf building blocks for both multiplayer development, no-ops deployment and delivery, blockchain integration and browser support.

## GameJam Topic

- Competitive or cooperative skill-based multiplayer for 2-4 players
- Theme: `INCLUSIVENESS`

## Notes

- Please work on this repository. Give access to your team members. We'll be most able to help you if your work is available on Github.
- 📄 Start with the [documentation](https://docs.elympics.cc/)
- 👾 Follow by our demo projects and [open source](https://github.com/Elympics/Elympics-Shooter). Get inspired!
- 🗣 Show & tell – share your ideas on Discord. We’d love to help and get involved ❤️ Be available! Be shameless about it!
- 🧪 Test full online gameplay as soon as possible. We’ll assist you in doing so!
- 🌓 Work with half remote mode from day one!

# Learn more

You'll need the following links for sure:

- [Documentation](https://docs.elympics.cc/)
- [Discord](https://discord.com/invite/VcBykPq4zd)
- [About the Jam](https://www.elympics.cc/elympicsjam)
